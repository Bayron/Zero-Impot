// @flow
import React from 'react';
import * as Font from 'expo-font';
import I18n from 'ex-react-native-i18n';
import { ApolloProvider, ApolloClient, createNetworkInterface } from 'react-apollo';

import translation from './app/config/locale/translation-fr';
import MainScreen from './app/navigation/MainScreen';
import * as firebase from 'firebase';

// Optionally import the services that you want to use
//import "firebase/auth";
//import "firebase/database";
//import "firebase/firestore";
//import "firebase/functions";
//import "firebase/storage";

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyDiqAN-qL_XRWyxreYXRg95POs5MQ7fJag",
    authDomain: "zero-impot-project.firebaseio.com",
    databaseURL: "https://zero-impot-project.firebaseio.com",
    projectId: "zero-impot-project",
    storageBucket: "zero-impot-project.appspot.com",
    messagingSenderId: "sender-id",
    appId: "1:147795985231:android:6851ec10d7e74ca5457b3a",
    measurementId: "G-measurement-id"
};

const client = new ApolloClient({
    networkInterface: createNetworkInterface({ uri: 'https://api.graph.cool/simple/v1/cj8j04ic004kh01306d1hnsa9'})
});

export default class App extends React.Component {
  state: {
    fontLoaded: boolean,
  };

  constructor(props) {
    super(props);
    firebase.initializeApp(firebaseConfig);
    this.state = {
      fontLoaded: false,
    };
  }

  componentDidMount() {
    Font.loadAsync({
      'Catamaran-Black': require('./assets/fonts/Catamaran-Black.ttf'),
      'Catamaran-Bold': require('./assets/fonts/Catamaran-Bold.ttf'),
      'Catamaran-ExtraBold': require('./assets/fonts/Catamaran-ExtraBold.ttf'),
      'Catamaran-ExtraLight': require('./assets/fonts/Catamaran-ExtraLight.ttf'),
      'Catamaran-Light': require('./assets/fonts/Catamaran-Light.ttf'),
      'Catamaran-Medium': require('./assets/fonts/Catamaran-Medium.ttf'),
      'Catamaran-Regular': require('./assets/fonts/Catamaran-Regular.ttf'),
      'Catamaran-SemiBold': require('./assets/fonts/Catamaran-SemiBold.ttf'),
      'Catamaran-Thin': require('./assets/fonts/Catamaran-Thin.ttf'),
  }).then(() => {
        I18n.fallbacks = true;
        I18n.translations = {
            fr: { translation },
        };
        I18n.locale = 'fr';
        this.setState({ fontLoaded: true });
    }).catch(() => console.log('Font not loaded'));
  }

  render() {
    if (!this.state.fontLoaded) return null;
    return (
        <MainScreen/>
    );
  }
}