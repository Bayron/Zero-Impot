import Const from "../config/Const";
import TaxLib from "./TaxLib";

export default class BouvardCensiLib {
    static getPrograms(bouvardCensi, taxAmount) {
        console.log("BouvardCensi-getPrograms:", bouvardCensi);
        const bouvardCensiAmount = this.getInvestment(taxAmount);
        const minPinelAmount = bouvardCensiAmount;
        const maxPinelAmount = bouvardCensiAmount + ((Const.PERCENT_PINEL / 100) * bouvardCensiAmount);
        let pinelMaxProgram = bouvardCensi.programs[0];
        let maxAppartment = bouvardCensi.programs[0].apartments[0];
        let programs = [];
        let isAlreadyIn = false;
        if (taxAmount < 2500) { return programs }
        bouvardCensi.programs.map(program => {
            isAlreadyIn = false;
            program.apartments.map(appartment => {
                if (appartment.price > maxAppartment) {
                    maxAppartment = appartment;
                    pinelMaxProgram = program;
                }
                if (isAlreadyIn === false &&
                    appartment.price >= minPinelAmount &&
                    appartment.price <= maxPinelAmount) {
                    programs.push(program);
                    isAlreadyIn = true
                }
            });
        });
        return programs.length === 0 ? [pinelMaxProgram] : programs;
    }

    static getAppartment(program, investment) {
        const maxPinelAmount = investment + ((Const.PERCENT_PINEL / 100) * investment);
        let appartments = [];
        let maxAppartment = program.apartments[0];
        program.apartments.map(appartment => {
            if (appartment.price >= investment) {
                if (appartment.price <= maxAppartment.price || maxAppartment.price < investment) {
                    maxAppartment = appartment;
                }
            }
            if (appartment.price >= investment && appartment.price <= maxPinelAmount) {
                appartments.push(appartment)
            }
        });
        return maxAppartment;
    }

    static getHorizon(investment) {
        const economy = investment * 0.11 + investment * 0.2;
        return {
            key: '0',
            duree: '9',
            economy: economy.toString(),
            saving: Math.round(economy / 9).toString(),
        };
    }

    static getInvestment(taxAmount) {
        const investment = Math.ceil(taxAmount * 82);
        const maxInvestment = Math.ceil(Const.MAX_LAW_VALUE.BOUVARD_CENSI * 82);
        return investment > maxInvestment ? Const.MAX_LAW_VALUE.BOUVARD_CENSI : investment;
    }

    static getTaxByInvestment(investment) {
        const taxAmount = investment / 82;
        return taxAmount > Const.MAX_LAW.BOUVARD_CENSI ? Const.MAX_LAW.BOUVARD_CENSI : taxAmount;
    }

    static getFinalAmount(taxAmount, investment) {
        const value = taxAmount - investment / 82;
        return value < 0 ? 0 : value;
    }

    static getObject(pinelLaw, taxAmount) {
        const investment = this.getInvestment(taxAmount);
        return {
            name: Const.LAW_NAME.BOUVARD_CENSI,
            investiment: investment.toString(),
            description: 'Ex pour 9 ans\n' +
                'Cette loi permet une réduction d’impôt\n' +
                'De 11% sur 9 ans. La loi Bouvard Censi peut être souscrite pour 9 ans',
            programs: this.getPrograms(pinelLaw, taxAmount),
            horizon: this.getHorizon(investment)
        }
    }

    static getActionSheetValue(investment) {
        return [
            150000,
            200000,
            250000,
            300000,
            investment
        ];
    }

    static getEpargne(rent, investment, taxAmount, gain, duree) {
        const adjustInvest = investment + (investment * 0.55);
        const finalInvest = adjustInvest > Const.MAX_LAW_VALUE.BOUVARD_CENSI ? Const.MAX_LAW_VALUE.BOUVARD_CENSI : adjustInvest;
        return TaxLib.getBasicEpargne(rent, adjustInvest, taxAmount, gain, duree);
    }
}