import React from 'react';
import { TouchableOpacity, Image, Alert } from 'react-native';
import images from "../../config/images";
import Const from "../../config/Const";

export default class HeaderRight extends React.Component {

    onClick() {
        Alert.alert('Voulez-vous vraiment revenir à l\'accueil ?', '', [
                { text: 'Oui', onPress: () => this.props.navigation.navigate('Splash') },
                { text: 'Non', style: 'cancel' }
            ],
            { cancelable: true });
    }

    render() {
        return (
            <TouchableOpacity
                style={{ marginRight: 15 }}
                onPress={() => this.onClick()}>
                <Image
                    style={{ tintColor: Const.COLOR.BLACKGREY }}
                    source={images.home}
                />
            </TouchableOpacity>
        )
    }
}