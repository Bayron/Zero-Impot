import React from "react";
import { View, Image, StyleSheet, Text } from "react-native";
import PropTypes from "prop-types";
import * as firebase from 'firebase';

import styles from "../config/styles";
import images from "../config/images";

const splashStyle = StyleSheet.create({
    image: {
        width: 300,
        height: 300,
    }
});

export default class Splash extends React.PureComponent {

    static propTypes = {
        navigation: PropTypes.shape({
            navigate: PropTypes.func,
        }),
    };

    state: {
        isLoading: Boolean,
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
        }
    }

    componentDidMount(): void {
        firebase.database().ref('/laws').on('value', (snapshot) => {
            const laws = this.snapshotToArray(snapshot);
            if (this.state.isLoading === false) {
                this.setState({ isLoading: true });
                this.props.navigation.navigate("Home", { laws });
            }
        });
    }

    snapshotToArray(snapshot) {
        let returnArr = [];

        snapshot.forEach(function(childSnapshot) {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;

            returnArr.push(item);
        });

        return returnArr;
    }

    render() {
        return (
            <View style={[styles.scrollView, styles.alignCenter, styles.justifyCenter]}>
                <Image
                    source={images.icon}
                    style={splashStyle.image}
                />
            </View>
        )
    }
}